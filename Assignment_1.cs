﻿/*
 * Author Đoàn Duy Tùng
 * Dev Mobile
 */

using System;
using System.Text;

namespace GettingStart
{
    class Assignment_1
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập tên của bạn vào đây!");
            Assignment_1 p = new Assignment_1();
            p.print(p.readInput());
            Console.ReadKey();
        }

        public String readInput()
        {
            return Console.ReadLine();
        }

        public void print(String name)
        {
            Console.WriteLine("Chào {0}! ", name);
        }
    }
}


/*
 * How do you read inputted data from console window?   | Sử dụng hàm Console.ReadLine()
 *                                                      |
 *-------------------------------------------------------------------------------------------------------------------------
 * What is difference when you use Console.Read         | Console.Read đọc mã ASII của kí tự cuối cùng sau khi nhấn Enter. 
 * vs Console.ReadKey                                   | Console.ReadKey đọc một kí tự từ bàn phím mà không phải nhấn Enter.
 * vs Console.ReadLine?                                 | Console.ReadLine đọc một chuỗi kí tự sau khi nhấn Enter;
 *                                                      |
 *  -----------------------------------------------------------------------------------------------------------------------
 *  How do you concatenate two or more strings in C#?   | Sử dụng cộng chuỗi ( vd: "a" + "b1" ---> "ab1")
 *                                                      | Sử dụng hàm String.Concat(Stirng1, String2)
 *                                                      |
 *                                                      |
 *                                                      |
 *                                                      |
 * 
 */